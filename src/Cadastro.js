import React, { useState } from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Image,
  Text,
  TouchableOpacity,
  TextInput,
  Alert,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useNavigation } from '@react-navigation/native';
import sheets from './axios'; 


export default function Cadastro() {
  const navigation = useNavigation(); 
  const [nome, setNome] = useState(""); 
  const [password, setPassword] = useState(""); 
  const [confirmPassword, setConfirmPassword] = useState("");
  const [telefone, setTelefone] = useState(""); 
  const [email, setEmail] = useState(""); 

  async function handleCadastrar() {

    // Validação se algum campo está vazio
    if (nome === "" || password === "" || confirmPassword === "" || telefone === "" || email === "") {
      Alert.alert("Erro", "Por favor, preencha todos os campos.");
      return;
    }

    // Validação do email
    if (!validateEmail(email)) {
      Alert.alert("Erro", "Por favor, insira um email válido.");
      return;
    }

    // Validação do telefone
    if (!validateTelefone(telefone)) {
      Alert.alert("Erro", "Por favor, insira um telefone válido.");
      return;
    }

    // Validação se as senhas coincidem
    if (password !== confirmPassword) {
      Alert.alert("Erro", "As senhas não coincidem.");
      return;
    }

    try {
      // Requisição de cadastro do usuário à API
      const response = await sheets.postCadastro(nome, password, confirmPassword, telefone, email);
      if (response.status === 200) {
        Alert.alert("Sucesso", response.data.message);
        navigation.navigate('paginavoos');
      }
    } catch (error) {
      if (error.response) {
        // Tratamento de erro se a API retornar um erro específico
        Alert.alert("Erro no cadastro", error.response.data.error);
      } else {
        Alert.alert("Erro de Conexão", "Erro ao conectar ao servidor.");
      }
    }
  };

  // Função para validar o formato do email
  function validateEmail(email) {
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailPattern.test(email);
  }

  // Função para validar o formato do telefone
  function validateTelefone(telefone) {
    const telefonePattern = /^\d{2}\s?\d{4,5}-?\d{4}$/;
    return telefonePattern.test(telefone);
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <KeyboardAwareScrollView>
          <View style={styles.header}>
            <Image
              alt="App Logo"
              resizeMode="contain"
              style={styles.headerImg}
              source={{
                uri: 'https://assets.withfra.me/SignIn.2.png',
              }} />

            <Text style={styles.title}>
              Crie sua <Text style={{ color: '#56409e' }}>conta</Text>
            </Text>

            <Text style={styles.text}>
              Comece a embarcar com a gente!
            </Text>
          </View>

          <View style={styles.form}>
            <View style={styles.input}>
              <Text style={styles.inputLabel}>Nome</Text>
              <TextInput
                placeholder="Seu nome"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                value={nome}
                onChangeText={setNome} />
            </View>

            <View style={styles.input}>
              <Text style={styles.inputLabel}>Email</Text>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="email-address"
                placeholder="email@exemplo.com"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                value={email}
                onChangeText={setEmail} />
            </View>

            <View style={styles.input}>
              <Text style={styles.inputLabel}>Telefone</Text>
              <TextInput
                keyboardType="phone-pad"
                placeholder="(XX) XXXXX-XXXX"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                value={telefone}
                onChangeText={setTelefone} />
            </View>

            <View style={styles.input}>
              <Text style={styles.inputLabel}>Senha</Text>
              <TextInput
                autoCorrect={false}
                placeholder="********"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                secureTextEntry={true}
                value={password}
                onChangeText={setPassword} />
            </View>

            <View style={styles.input}>
              <Text style={styles.inputLabel}>Confirmar Senha</Text>
              <TextInput
                autoCorrect={false}
                placeholder="********"
                placeholderTextColor="#6b7280"
                style={styles.inputControl}
                secureTextEntry={true}
                value={confirmPassword}
                onChangeText={setConfirmPassword} />
            </View>

            <View style={styles.formAction}>
              <TouchableOpacity onPress={handleCadastrar}>
                <View style={styles.button}>
                  <Text style={styles.buttonText}>Cadastrar</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 24,
    paddingHorizontal: 0,
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
  },
  title: {
    fontSize: 28,
    fontWeight: '500',
    color: '#281b52',
    textAlign: 'center',
    marginBottom: 12,
    lineHeight: 40,
  },
  text: {
    fontSize: 15,
    lineHeight: 24,
    fontWeight: '400',
    color: '#9992a7',
    textAlign: 'center',
  },
  header: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 36,
  },
  headerImg: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    marginBottom: 36,
  },
  form: {
    marginBottom: 24,
    paddingHorizontal: 24,
    flexGrow: 1,
    flexShrink: 1,
    flexBasis: 0,
  },
  formAction: {
    marginTop: 4,
    marginBottom: 16,
  },
  formLink: {
    fontSize: 16,
    fontWeight: '600',
    color: '#075eec',
    textAlign: 'center',
  },
  formFooter: {
    fontSize: 15,
    fontWeight: '600',
    color: '#222',
    textAlign: 'center',
    letterSpacing: 0.15,
  },
  input: {
    marginBottom: 16,
  },
  inputLabel: {
    fontSize: 17,
    fontWeight: '600',
    color: '#222',
    marginBottom: 8,
  },
  inputControl: {
    height: 50,
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    borderRadius: 12,
    fontSize: 15,
    fontWeight: '500',
    color: '#222',
    borderWidth: 1,
    borderColor: '#C9D3DB',
    borderStyle: 'solid',
  },
  button: {
    backgroundColor: '#56409e',
    paddingVertical: 12,
    paddingHorizontal: 14,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 12,
  },
  buttonText: {
    fontSize: 15,
    fontWeight: '500',
    color: '#fff',
  },
});
