import axios from 'axios';

const api = axios.create({
    baseURL: "http://10.89.234.166:8081/passagem/",
    headers: {
        'accept': 'application/json'
    },
});

const sheets = {
     // requisição de login de usuário
    postUser: (email, password) =>
    api.post("/Logincliente", email, password),

    // requisição de cadastro de usuário
    postCadastro: (nome, password, confirmPassword, telefone, email) => 
    api.post("/cliente", nome, password, confirmPassword, telefone, email),
};

export default sheets;