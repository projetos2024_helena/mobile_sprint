import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';

// Componente para a página de listagem de voos
const PaginaVoos = () => {
  const navigation = useNavigation(); 

  // Função para voltar para a página anterior
  const handleBack = () => {
    navigation.goBack();
  };

  // Função para lidar com o clique em uma passagem aérea para mostrar detalhes
  const handleDetalhesPassagem = (passagem) => {
    console.log("Detalhes da passagem:", passagem);
  };

  // Array de passagens aéreas simuladas
  const passagens = [
    { 
      numero: "124", 
      companhiaAerea: "Companhia B", 
      origem: "Origem2", 
      destino: "Destino2", 
      preco: "$200", 
      data: "2024-01-02",
      hora: "13:00:00",
      classe: "Executiva"
    },
    { 
      numero: "125", 
      companhiaAerea: "Companhia C", 
      origem: "Origem3", 
      destino: "Destino3", 
      preco: "$300",
      data: "2024-01-03",
      hora: "14:00:00",
      classe: "Primeira Classe"
    },
    { 
      numero: "126", 
      companhiaAerea: "Companhia D", 
      origem: "Origem4", 
      destino: "Destino4", 
      preco: "$150", 
      data: "2024-01-04",
      hora: "15:00:00",
      classe: "Econômica"
    },
    { 
      numero: "127,", 
      companhiaAerea: "Companhia E", 
      origem: "Origem5", 
      destino: "Destino5", 
      preco: "$250", 
      data: "2024-01-05",
      hora: "16:00:00",
      classe: "Executiva"
    },
  ];

  // Componente para renderizar cada caixa de passagem aérea
  const PassagemAereaBox = ({ passagem, onPress }) => (
    <View style={styles.box}>
      <Text style={styles.label}>Número: {passagem.numero}</Text>
      <Text style={styles.label}>Companhia Aérea: {passagem.companhiaAerea}</Text>
      <Text style={styles.label}>Origem: {passagem.origem}</Text>
      <Text style={styles.label}>Destino: {passagem.destino}</Text>
      <Text style={styles.label}>Data: {passagem.data}</Text>
      <Text style={styles.label}>Hora: {passagem.hora}</Text>
      <Text style={styles.label}>Classe: {passagem.classe}</Text>
      <Text style={styles.label}>Preço: {passagem.preco}</Text>
      <TouchableOpacity style={styles.button} onPress={() => onPress(passagem)}>
        <Text style={styles.buttonText}>Reservar</Text>
      </TouchableOpacity>
    </View>
  );

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={handleBack}>
          <Text style={styles.text}>Voltar</Text>
        </TouchableOpacity>
      </View>
      <Text style={styles.message}>
        Escolha a sua passagem
      </Text>
      <ScrollView style={{ width: '100%' }}>
        {passagens.map((passagem, index) => (
          <PassagemAereaBox
            key={index}
            passagem={passagem}
            onPress={handleDetalhesPassagem}
          />
        ))}
      </ScrollView>
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  header: {
    marginBottom: 20,
  },
  text: {
    fontSize: 15,
    color: "#56409e",
  },
  message: {
    fontSize: 20,
    marginBottom: 20,
  },
  box: {
    backgroundColor: "#dfdfdf",
    padding: 20,
    marginBottom: 20,
    borderRadius: 10,
    width: '100%',
  },
  label: {
    fontSize: 16,
    marginBottom: 5,
  },
  button: {
    backgroundColor: "#56409e",
    paddingVertical: 9,
    paddingHorizontal: 8,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 12,
    marginTop: 10,
    width: '100%',
  },
  buttonText: {
    fontSize: 15,
    color: "#fff",
  },
});

export default PaginaVoos;
