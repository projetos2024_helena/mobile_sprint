import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/HomeScreen';
import LoginScreen from './src/Login';
import CadastroScreen from './src/Cadastro';
import PaginavoosScreen from './src/paginavoos';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Cadastro" component={CadastroScreen} />
        <Stack.Screen name="paginavoos" component={PaginavoosScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
